#!/bin/bash
set -x
######### Install main components that need to run in every node ##########

# Update and Upgrade the system. Install some useful tools
yum update -y
yum install wget vim net-tools -y

# Load overlay and br_netfilter modprobe modules. Necessary for future steps
modprobe overlay
modprobe br_netfilter

# Update kernel networking to allow necessary traffic
cat <<EOF | tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF

# Ensure the changes are used by the current kernel as well
sysctl --system

# Disable SELinux to allow smooth communication between the nodes and pods
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

# Get containerd binaries from a tar file
wget https://github.com/containerd/containerd/releases/download/v1.7.11/containerd-1.7.11-linux-amd64.tar.gz
tar Cxzvf /usr/local/ containerd-1.7.11-linux-amd64.tar.gz

# Get containerd.service unit file and make it work as a systemctl service
wget https://raw.githubusercontent.com/containerd/containerd/main/containerd.service
mv containerd.service /etc/systemd/system/.
chcon -t systemd_unit_file_t /etc/systemd/system/containerd.service

# Reload systemctl daemon and enable containerd.service
systemctl daemon-reload
systemctl enable --now containerd

# Download runc for your system
wget https://github.com/opencontainers/runc/releases/download/v1.1.11/runc.amd64

# Install runc in /usr/local/sbin/runc
install -m 755 runc.amd64 /usr/local/sbin/runc

# Download cni plugins .tgz file
wget https://github.com/containernetworking/plugins/releases/download/v1.4.0/cni-plugins-linux-amd64-v1.4.0.tgz

# Install cni plugins in /opt/cni/bin
mkdir -p /opt/cni/bin
tar Cxzvf /opt/cni/bin cni-plugins-linux-amd64-v1.4.0.tgz

# Create folder for configuration
mkdir -p /etc/containerd

# Created the default configuration file
containerd config default | tee /etc/containerd/config.toml

# Enable SystemdCgroup in the configuration file
sed -i 's/SystemdCgroup = false/SystemdCgroup = true/' /etc/containerd/config.toml

# Restart containerd
systemctl restart containerd

# Load kubernetes public repository for installing k8s bundle: kubeadm, kubelet, kubectl
cat <<EOF | tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://pkgs.k8s.io/core:/stable:/v1.29/rpm/
enabled=1
gpgcheck=1
gpgkey=https://pkgs.k8s.io/core:/stable:/v1.29/rpm/repodata/repomd.xml.key
exclude=kubelet kubeadm kubectl cri-tools kubernetes-cni
EOF

# Install kubelet, kubeadm and kubectl
yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
systemctl enable --now kubelet
